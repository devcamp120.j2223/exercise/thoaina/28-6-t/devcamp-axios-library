import "bootstrap/dist/css/bootstrap.min.css";
import AxiosAPI from "./components/AxiosAPI";

function App() {
  return (
    <div className="container">
      <AxiosAPI />
    </div>
  );
}

export default App;
