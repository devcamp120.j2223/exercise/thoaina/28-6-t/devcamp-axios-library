import { Component } from "react";
import axios from "axios";

class AxiosAPI extends Component {
    axiosAPI = async (url, body) => {
        let res = await axios(url, body);
        return res.data;
    }

    getAllAPI = () => {
        console.log("Get All API");
        this.axiosAPI("https://jsonplaceholder.typicode.com/posts")
            .then((data) => console.log(data))
    }

    getAPIById = () => {
        console.log("Get API by ID");
        this.axiosAPI("https://jsonplaceholder.typicode.com/posts/1")
            .then((data) => console.log(data))
    }

    createAPI = () => {
        let body = {
            method: 'POST',
            data: JSON.stringify({
                title: 'NODE',
                body: 'bar',
                userId: 100,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        console.log("Create API");
        this.axiosAPI("https://jsonplaceholder.typicode.com/posts", body)
            .then((data) => console.log(data))
    }

    updateAPI = () => {
        console.log("Update API");
        let body = {
            method: 'PUT',
            data: JSON.stringify({
                id: 1,
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.axiosAPI("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => console.log(data))
    }

    deleteAPI = () => {
        console.log("Delete API");
        let body = {
            method: "DELETE"
        }
        this.axiosAPI("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => console.log(data));
    }

    render() {
        return (
            <div className="row mt-5">
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.getAllAPI}>Get All API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.getAPIById}>Get API by ID</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.createAPI}>Create API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.updateAPI}>Update API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.deleteAPI}>Delete API</button>
                </div>
            </div>
        )
    }
}

export default AxiosAPI;
